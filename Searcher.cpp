

#include "Searcher.h"

Searcher::Searcher() {
    this->priorityQueue = new MyPriorityQueue();
}

State *Searcher::popPriorityQueue() {
    evaluatedNodes++;
    State *state = priorityQueue->top();
    priorityQueue->pop();
    return state;
}

int Searcher::priorityQueueSize() {
    return priorityQueue->size();
}


int Searcher::getNumberOfNodesEvaluated() {
    return evaluatedNodes;
}

double Searcher::getFinalRouteSum() const {
    return finalRouteSum;
}

Searcher::~Searcher() {
    delete (priorityQueue);
}
