
#ifndef SERVER_SIDE_SOLVERSEARCHER_H
#define SERVER_SIDE_SOLVERSEARCHER_H

#include <vector>
#include "Solver.h"
#include "SearcherInterface.h"
#include "SearchableStateMatrix.h"
#include <sstream>

class SolverSearcher : public Solver<SearchableInterface *, SearchResult *> {

    SearcherInterface *searcher;

public:
    SolverSearcher(SearcherInterface *searcher) : searcher(searcher) {}

    SearchResult *solve(SearchableInterface *searchableInterface) override {
        return searcher->search(searchableInterface);
    }

    virtual ~SolverSearcher() {
        delete (searcher);
    }

};


#endif //SERVER_SIDE_SOLVERSEARCHER_H