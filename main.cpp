
#include <vector>
#include <string>
#include "SearchableInterface.h"
#include "SearchableStateMatrix.h"
#include "SearcherInterface.h"
#include "DepthFirstSearch.h"
#include "BreadthFirstSearch.h"
#include "BestFirstSearch.h"
#include "AStar.h"
#include "CacheManager.h"
#include "FileCacheManager.h"
#include "Solver.h"
#include "MyClientHandler.h"
#include "MyParallelServer.h"
#include "Solver.h"
#include "SolverSearcher.h"


using namespace std;


int main(int argc, char *argv[]) {
    int port;

    if (argc > 1) {
        port = stoi(argv[1]);
    } else {
        port = 5400;
    }

    CacheManager *cacheManager = new FileCacheManager("memory");
    auto solver = new SolverSearcher(new AStar());
    ClientHandler *clientHandler = new MyClientHandler(solver, cacheManager);
    ServerInterface *server = new MyParallelServer();
    server->open(port, clientHandler);


    delete (clientHandler);
    delete (server);
}



