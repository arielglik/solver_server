

#ifndef SERVER_SIDE_SOLVER_H
#define SERVER_SIDE_SOLVER_H

#include "string"

using namespace std;

template<class Problem, class Solution>
class Solver {

public:
    virtual Solution solve(Problem problem) = 0;

    virtual ~Solver() {}
};

#endif //SERVER_SIDE_SOLVER_H
