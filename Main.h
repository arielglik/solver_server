

#ifndef SERVER_SIDE_MAIN_H
#define SERVER_SIDE_MAIN_H

namespace boot {
    class Main {

    public:
        int main(void *args);
    };

}


#endif //SERVER_SIDE_MAIN_H
