

#ifndef SERVERINTERFACE_H
#define SERVERINTERFACE_H

#include "ClientHandler.h"
#include <stdio.h>
#include <stdlib.h>
#include <netdb.h>
#include <sys/socket.h>
#include <unistd.h>
#include <netinet/in.h>
#include <strings.h>


using namespace std;

namespace server_side {

    class ServerInterface {

    private:


    public:
        virtual void open(int port, ClientHandler *clientHandler) = 0;

        virtual void closeServer(int sockfd) = 0;
    };
};
using namespace server_side;

#endif //SERVERINTERFACE_H
