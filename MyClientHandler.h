

#ifndef SERVER_SIDE_MYCLIENTHANDLER_H
#define SERVER_SIDE_MYCLIENTHANDLER_H

#include <vector>
#include <algorithm>
#include "ClientHandler.h"
#include "Solver.h"
#include "CacheManager.h"
#include "SearchableInterface.h"
#include "SearcherInterface.h"
#include "AStar.h"


class MyClientHandler : public ClientHandler {

private:
    Solver<SearchableInterface *, SearchResult *> *solver;
    CacheManager *cm;

public:
    void handleClient(int sockfd) override;

    string proplemReader(int sockfd);

    void solutionWriter(int sockfd, string solution);

    SearchableInterface *createSearchableMatrix(string str);

    MyClientHandler(Solver<SearchableInterface *, SearchResult *> *solver, CacheManager *cm);

    virtual ~MyClientHandler();
};


#endif //SERVER_SIDE_MYCLIENTHANDLER_H
