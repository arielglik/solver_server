

#ifndef SERVER_SIDE_CLIENTHANDLER_H
#define SERVER_SIDE_CLIENTHANDLER_H

#include <fstream>

using namespace std;

class ClientHandler {

private:

public:
    virtual void handleClient(int sockfd) = 0;

    virtual ~ClientHandler() {};
};

#endif //SERVER_SIDE_CLIENTHANDLER_H
