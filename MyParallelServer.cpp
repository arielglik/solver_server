
#include "MyParallelServer.h"

/**
 * preHandle
 * @param arg
 * @return current socket
 */
void *preHandle(void *arg) {
    struct ThreadParams *params = (struct ThreadParams *) arg;
    params->clientHandler->handleClient(params->sockid);
    params->isFinished = true;
}

/**
 * open
 * accept the socket and open thread
 */
void server_side::MyParallelServer::open(int port, ClientHandler *clientHandler) {
    int sockfd = socket(AF_INET, SOCK_STREAM, 0);
    struct sockaddr_in serv;
    int new_sock;
    bool flag = false;

    struct sockaddr_in client;

    serv.sin_addr.s_addr = INADDR_ANY;

    serv.sin_port = htons(port);
    serv.sin_family = AF_INET;
    if (bind(sockfd, (sockaddr *) &serv, sizeof(serv)) < 0) {
        cerr << "Bad!" << endl;
    }
    timeval timeout;
    timeout.tv_sec = 10;
    timeout.tv_usec = 0;

    listen(sockfd, 10);
    socklen_t clilen = sizeof(client);

    while (true) {
        if (flag) {
            setsockopt(sockfd, SOL_SOCKET, SO_RCVTIMEO, (char *) &timeout, sizeof(timeout));
        }
        flag = true;

        new_sock = accept(sockfd, (struct sockaddr *) &client, &clilen);

        if (new_sock < 0) {
            int error = errno;
            if (errno == EWOULDBLOCK) { // timeout
                break;
            } else if (errno == 4) {
                continue;
            } else {
                perror("other error");
                exit(3);
            }
        }
        ThreadParams *threadParams = new ThreadParams(new_sock, clientHandler);
        this->threadParamsList.push_back(threadParams);

        pthread_t trid;
        pthread_create(&trid, nullptr, preHandle, (void *) threadParams);
    }
    closeServer(sockfd);
}

/**
 * closeServer
 * close the socket
 */
void server_side::MyParallelServer::closeServer(int sockfd) {
    int counter = 0;
    list<ThreadParams *>::iterator it = this->threadParamsList.begin();
    while (true) {
        for (; it != threadParamsList.end(); it++) {
            if ((*it)->isFinished) {
                counter++;
            }
        }
        if (counter == this->threadParamsList.size()) {
            break;
        }
        it = this->threadParamsList.begin();
        counter = 0;
    }
    for (it = this->threadParamsList.begin(); it != threadParamsList.end(); it++) {
        delete (*it);
    }
    threadParamsList.clear();
    close(sockfd);
}


server_side::MyParallelServer::MyParallelServer() {}


ThreadParams::ThreadParams(int sockid, ClientHandler *clientHandler) : sockid(sockid), clientHandler(clientHandler) {
    isFinished = false;
}

ThreadParams::~ThreadParams() {}

