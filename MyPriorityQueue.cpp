#include "MyPriorityQueue.h"

/**
 * compareBySumRoute
 * cmp func by sum-route
 */
bool compareBySumRoute(State *s1, State *s2) {
    return (s1->getSumRoute() > s2->getSumRoute());
}

/**
 * compareByFcost
 * cmp func by Fcost
 */
bool compareByFcost(State *s1, State *s2) {
    return (s1->getFCost() > s2->getFCost());
}

/**
 * pop
 * pop the priority queue
 */
void MyPriorityQueue::pop() {
    State *state = priorityList.back();
    if (stateSet.find(state) != stateSet.end()) {
        stateSet.erase(state);
    }
    priorityList.pop_back();
}
/**
 * top of PriorityQueue
 */
State *MyPriorityQueue::top() {
    State *state = priorityList.back();
    return state;
}

/**
 * push of PriorityQueue
 */
void MyPriorityQueue::push(State *s) {
    priorityList.push_back(s);
    stateSet.insert(s);
    adjust();
}

int MyPriorityQueue::size() {
    return priorityList.size();
}

/**
 * check if the state is in the queue
 */
bool MyPriorityQueue::contains(State *s) {

    if (stateSet.find(s) == stateSet.end()) {
        return false;
    }
    return true;
}

/**
 * sort the queue by the cmp func
 */
void MyPriorityQueue::adjust() {
    switch (compareType) {
        case 0:
            priorityList.sort(compareBySumRoute);
            break;

        case 1:
            priorityList.sort(compareByFcost);
            break;

        default:
            priorityList.sort(compareBySumRoute);
            break;
    }
}

bool MyPriorityQueue::isEmpty() {
    return !(priorityList.size() > 0);
}

void MyPriorityQueue::setCompareType(int compareType) {
    MyPriorityQueue::compareType = compareType;
}

MyPriorityQueue::~MyPriorityQueue() {
    priorityList.clear();
    stateSet.clear();
}
