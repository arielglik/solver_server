

#ifndef SERVER_SIDE_BREADTHFIRSTSEARCH_H
#define SERVER_SIDE_BREADTHFIRSTSEARCH_H


#include "Searcher.h"

class BreadthFirstSearch : public Searcher {

public:
    SearchResult *search(SearchableInterface *searchable) override;

    BreadthFirstSearch();
};


#endif //SERVER_SIDE_BREADTHFIRSTSEARCH_H
