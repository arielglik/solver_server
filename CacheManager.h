
#ifndef SERVER_SIDE_CACHEMANAGER_H
#define SERVER_SIDE_CACHEMANAGER_H

#include <string>
#include "string.h"

using namespace std;

class CacheManager {

public:
    virtual string getSolvedProblem(string problem) = 0;

    virtual void saveSolution(string problem, string solution) = 0;

    virtual void loadData() = 0;

    virtual void saveMap() = 0;

    virtual ~CacheManager() {}
};


#endif //SERVER_SIDE_CACHEMANAGER_H
