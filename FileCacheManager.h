
#ifndef SERVER_SIDE_FILECACHEMANAGER_H
#define SERVER_SIDE_FILECACHEMANAGER_H

#include <tr1/unordered_map>
#include <mutex>
#include "CacheManager.h"
#include "string"
#include <fstream>

using namespace tr1;
using namespace std;


class FileCacheManager : public CacheManager {

private:
    unordered_map<string, string> probAndSolMap;
    string file;
    bool isMapLoaded;

public:
    void  loadData() override;

    void saveMap() override;

    string getSolvedProblem(string problem) override;

    void saveSolution(string problem, string solution) override;

    FileCacheManager(const string &file);

    virtual ~FileCacheManager();
};

#endif //SERVER_SIDE_FILECACHEMANAGER_H

