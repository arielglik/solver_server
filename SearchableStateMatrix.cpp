

#include "SearchableStateMatrix.h"

#define CALC_HEUR(lenA,lenB, a, b) (lenA-a+lenB-b-2)
/**
 * SearchableStateMatrix
 * @param userMatrix
 */
SearchableStateMatrix::SearchableStateMatrix(vector<string> userMatrix) : userMatrix(userMatrix) {
    int len = userMatrix.at(0).length();
    string str = userMatrix.at(0);
    int counter = 1;
    for (int i = 0; i < len; i++) {
        if (str[i] == ',') {
            counter++;
        }
    }

    this->matrixCols = counter;
    this->matrixRows = userMatrix.size()-2;

    createStateMatrix();
    initalizeGoalAndInitialStates();
}

/**
 * ~SearchableStateMatrix
 */
SearchableStateMatrix::~SearchableStateMatrix() {
    userMatrix.clear();

    for (int i = 0; i < matrixRows; i++) {
        for (int j = 0; j < matrixCols; j++) {
            delete (stateMatrix.at(i).at(j));
        }
        stateMatrix.at(i).empty();
    }
    stateMatrix.empty();
}

/**
 * createStateMatrix
 */
void SearchableStateMatrix::createStateMatrix() {
    State *state;
    string line, k;
    int i, j;
    vector<string> vec;
    vector<State *> tempStateVec;


    for (i = 0; i < matrixRows; i++) {
        line = userMatrix.at(i);
        std::stringstream ss(line);
        std::string to;

        //split by lines
        while (std::getline(ss, to, ',')) {
            vec.push_back(to);
        }

        for (j = 0; j < matrixCols; j++) {
            state = new State(new RowAndCol(i, j), stod(vec.at(j)), CALC_HEUR(matrixRows,matrixCols, i, j));
            tempStateVec.push_back(state);
        }
        stateMatrix.push_back(tempStateVec);
        vec.clear();
        tempStateVec.clear();
    }
}
/**
 * initalizeGoalAndInitialStates
 * set the goal and the initial states
 */
void SearchableStateMatrix::initalizeGoalAndInitialStates() {
    string line, row, col;
    RowAndCol *rowAndCol;

    line = userMatrix.at(matrixRows); // initialState
    rowAndCol = getRowAndColOfState(line);
    this->initialState = stateMatrix.at(rowAndCol->row).at(rowAndCol->col);
    delete (rowAndCol);

    line = userMatrix.at(matrixRows + 1); // goalState
    rowAndCol = getRowAndColOfState(line);
    this->goalState = stateMatrix.at(rowAndCol->row).at(rowAndCol->col);
    delete (rowAndCol);
}


State *SearchableStateMatrix::getInitialState() {
    return initialState;
}

State *SearchableStateMatrix::getGoalState() {
    return goalState;
}

/**
 * getAllPossibleStates
 * @param s
 * @return possible states to move from the current state
 */
list<State *> SearchableStateMatrix::getAllPossibleStates(State *s) {
    RowAndCol *rowAndCol = s->getState();
    list<State *> lst;
    State *state;
    int row = rowAndCol->row;
    int col = rowAndCol->col;


    if (row != 0) { //up
        state = stateMatrix.at(row - 1).at(col);
        if (state->getCost() != -1 && !state->getIsVisited()) {
            lst.push_back(state);
        }
    }
    if (row != matrixRows - 1) { //down
        state = stateMatrix.at(row + 1).at(col);
        if (state->getCost() != -1 && !state->getIsVisited()) {
            lst.push_back(state);
        }
    }
    if (col != 0) { //left
        state = stateMatrix.at(row).at(col - 1);
        if (state->getCost() != -1 && !state->getIsVisited()) {
            lst.push_back(state);
        }
    }
    if (col != matrixCols - 1) { //right
        state = stateMatrix.at(row).at(col + 1);
        if (state->getCost() != -1 && !state->getIsVisited()) {
            lst.push_back(state);
        }
    }

    return lst;
}

/**
 * backTrace
 * return the trace from the start state to goal state
 */
SearchResult *SearchableStateMatrix::backTrace(State *s) {
    string trace = "";
    vector<string> vec;
    State *curState = s;
    State *cameFrom = curState->getCameFrom();
    RowAndCol *rowAndColFather;
    RowAndCol *rowAndColSon;
    int routeLen = 1;
    double routePrice = 0;

    while (cameFrom != NULL) {
        rowAndColFather = cameFrom->getState();
        rowAndColSon = curState->getState();

        if (rowAndColFather->row - rowAndColSon->row == -1) {
            vec.push_back("Down");
        } else if (rowAndColFather->row - rowAndColSon->row == 1) {
            vec.push_back("Up");
        } else if (rowAndColFather->col - rowAndColSon->col == -1) {
            vec.push_back("Right");
        } else if (rowAndColFather->col - rowAndColSon->col == 1) {
            vec.push_back("Left");
        }
        routeLen++;
        routePrice += curState->getCost();
        curState = cameFrom;
        cameFrom = curState->getCameFrom();
    }
    routePrice += curState->getCost();

    for (int i = vec.size() - 1; i > 0; i--) {
        trace += vec.at(i) + ",";
    }
    trace += vec.at(0);
    SearchResult *searchResult = new SearchResult(trace, routePrice, routeLen);
    return searchResult;
}

RowAndCol *SearchableStateMatrix::getRowAndColOfState(string str) {
    int i, j, row, col;

    for (i = 0; str[i] != ',' && i < str.length(); i++) {}
    row = stoi(str.substr(0, i));
    i++;
    for (j = i; j < str.length(); j++) {}
    col = stoi(str.substr(i, j - i));


    return new RowAndCol(row, col);
}

RowAndCol::RowAndCol(int row, int col) : row(row), col(col) {}
