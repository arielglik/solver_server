
#ifndef SERVER_SIDE_MYPRIORITYQUEUE_H
#define SERVER_SIDE_MYPRIORITYQUEUE_H

#include <list>
#include <set>
#include "State.h"

enum compareType {
    CMP_BY_SUMROUTE = 0,
    CMP_BY_FCOST = 1,
};

class MyPriorityQueue {

private:
    list<State *> priorityList;
    set<State *> stateSet;
    int compareType;

public:
    void pop();

    State *top();

    void push(State *s);

    int size();

    bool contains(State *s);

    void adjust();

    bool isEmpty();

    void setCompareType(int compareType);

    virtual ~MyPriorityQueue();
};


#endif //SERVER_SIDE_MYPRIORITYQUEUE_H
