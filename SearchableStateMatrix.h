
#ifndef SERVER_SIDE_SEARCHABLESTATEMATRIX_H
#define SERVER_SIDE_SEARCHABLESTATEMATRIX_H

#include <string>
#include <vector>
#include "State.h"
#include "SearchableInterface.h"
#include <sstream>

using namespace std;


class SearchableStateMatrix : public SearchableInterface {

    int matrixCols;
    int matrixRows;
    vector<string> userMatrix;
    vector<vector<State *>> stateMatrix;
    State *initialState;
    State *goalState;


public:
    SearchableStateMatrix(vector<string> userMatrix);

    void createStateMatrix();

    void initalizeGoalAndInitialStates();

    RowAndCol *getRowAndColOfState(string str);

    State *getInitialState() override;

    State *getGoalState() override;

    list<State *> getAllPossibleStates(State *s) override;

    SearchResult *backTrace(State *s) override;

    ~SearchableStateMatrix();
};


#endif //SERVER_SIDE_SEARCHABLESTATEMATRIX_H
