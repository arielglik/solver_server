

#include "BreadthFirstSearch.h"
/**
 * search by BreadthFirstSearch
 * @param searchable
 * @return search the graph
 */
SearchResult *BreadthFirstSearch::search(SearchableInterface *searchable) {
    State *s;
    list<State *> adj;
    list<State *> queue;
    bool flag = false;
    string solution;
    int evalCounter = 1;
    SearchResult *searchResult;

    // Mark the current node as visited and enqueue it
    searchable->getInitialState()->setIsVisited(true);
    queue.push_back(searchable->getInitialState());

    // 'i' will be used to get all adjacent
    // vertices of a vertex
    list<State *>::iterator it;
    State *src = searchable->getInitialState();
    State *goal = searchable->getGoalState();
    src->setIsVisited(true);
    queue.push_back(src);

    // standard BFS algorithm
    while (!queue.empty()) {
        s = queue.front();
        queue.pop_front();

        adj = searchable->getAllPossibleStates(s);
        for (it = adj.begin(); it != adj.end(); ++it) {
            if (!(*it)->getIsVisited()) {
                (*it)->setIsVisited(true);
                (*it)->setCameFrom(s);
                queue.push_back(*it);
                evalCounter++;

                // save solution
                if (*(*it)->getState() == *goal->getState() && !flag) {
                    searchResult = searchable->backTrace(*it);
                    this->finalRouteSum = searchResult->shortestPathWeight;
                    this->evaluatedNodes = searchResult->developedVertices;
                    flag = true;
                }
            }
        }
    }
    return searchResult;
}

BreadthFirstSearch::BreadthFirstSearch() {}

