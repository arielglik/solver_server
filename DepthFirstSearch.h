
#ifndef SERVER_SIDE_DEPTHFIRSTSEARCH_H
#define SERVER_SIDE_DEPTHFIRSTSEARCH_H


#include "Searcher.h"

class DepthFirstSearch : public Searcher {

    bool isSolutionFound;
    SearchResult *searchResult;

public:
    SearchableInterface *searchable;

    SearchResult *search(SearchableInterface *searchable) override;

    void recursiveDFS(State *s);

    DepthFirstSearch();

    virtual ~DepthFirstSearch();
};


#endif //SERVER_SIDE_DEPTHFIRSTSEARCH_H
