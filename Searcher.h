

#ifndef SERVER_SIDE_SEARCHER_H
#define SERVER_SIDE_SEARCHER_H

#include "SearcherInterface.h"
#include <queue>
#include "MyPriorityQueue.h"
#include "SearchableStateMatrix.h"

class Searcher : public SearcherInterface {

protected:
    MyPriorityQueue *priorityQueue;
    int evaluatedNodes;
    double finalRouteSum;

    State *popPriorityQueue();

public:
    Searcher();

    int priorityQueueSize();

    virtual SearchResult *search(SearchableInterface *searchable) = 0;

    int getNumberOfNodesEvaluated() override;

    double getFinalRouteSum() const;

    virtual ~Searcher();
};


#endif //SERVER_SIDE_SEARCHER_H
