

#ifndef SERVER_SIDE_STATE_H
#define SERVER_SIDE_STATE_H

#include <string>
#include <cfloat>

using namespace std;
/**
 * struct that represent the row and col of the state
 */
struct RowAndCol {
    int row;
    int col;

    RowAndCol(int row, int col);

    bool operator==(const RowAndCol &r) const {
        if (this->row == r.row && this->col == r.col) {
            return true;
        }
        return false;
    }
};

class State {

protected:
    RowAndCol *state;
    double cost;
    double sumRoute;
    double hCost;
    double fCost;
    State *cameFrom;
    bool isVisited;

public:

    State(RowAndCol *state, double cost, double hCost) : state(state), cost(cost), sumRoute(cost), hCost(hCost) {
        isVisited = false;
        cameFrom = NULL;
        fCost = cost + hCost;
    }

    RowAndCol *getState() const {
        return state;
    }

    double getCost() const {
        return cost;
    }

    State *getCameFrom() const {
        return cameFrom;
    }

    void setCameFrom(State *cameFrom) {
        this->cameFrom = cameFrom;
    }

    bool getIsVisited() const {
        return isVisited;
    }

    void setIsVisited(bool isVisited) {
        this->isVisited = isVisited;
    }

    double getSumRoute() const {
        return sumRoute;
    }

    void addSumRoute(double sumRoute) {
        State::sumRoute += sumRoute;
    }

    void setSumRoute(double sumRoute) {
        State::sumRoute = sumRoute;
        fCost = sumRoute + hCost;
    }

    void setFCost(double fCost) {
        State::fCost = fCost;
    }

    double getHCost() const {
        return hCost;
    }

    double getFCost() const {
        return fCost;
    }

    virtual ~State() {
        delete (state);
    }

    bool operator==(const State &s) const {
        if (this->getState() == s.getState()) {
            return true;
        }
        return false;
    }
};

#endif //SERVER_SIDE_STATE_H
