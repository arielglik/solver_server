
#ifndef SERVER_SIDE_SEARCHABLEINTERFACE_H
#define SERVER_SIDE_SEARCHABLEINTERFACE_H

#include "State.h"
#include <list>

using namespace std;

struct SearchResult {

    string shortestPathRoute;

    double shortestPathWeight;

    int developedVertices;

    SearchResult(const string &shortestPathRoute, int shortestPathWeight, int developedVertices) : shortestPathRoute(
            shortestPathRoute), shortestPathWeight(shortestPathWeight), developedVertices(developedVertices) {}
};

class SearchableInterface {

public:
    virtual State *getInitialState() = 0;

    virtual State *getGoalState() = 0;

    virtual list<State *> getAllPossibleStates(State *s) = 0;

    virtual SearchResult *backTrace(State *s) = 0;

    virtual ~SearchableInterface() {};
};


#endif //SERVER_SIDE_SEARCHABLEINTERFACE_H
