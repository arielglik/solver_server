
#include "BestFirstSearch.h"

/**
 * search by BestFirstSearch
 * @param searchable
 * @return the shortest path
 */
SearchResult *BestFirstSearch::search(SearchableInterface *searchable) {
    priorityQueue->setCompareType(CMP_BY_SUMROUTE);
    priorityQueue->push(searchable->getInitialState());
    set<State *> closed;
    State *goal = searchable->getGoalState();
    double sumUntil;

    while (priorityQueueSize() > 0) {
        State *n = popPriorityQueue();
        closed.insert(n);
        n->setIsVisited(true);

        // if this node is the target node
        if (*n == *goal) {
            SearchResult *searchResult = searchable->backTrace(n);
            this->finalRouteSum = searchResult->shortestPathWeight;
            this->evaluatedNodes = searchResult->developedVertices;
            return searchResult;
        }

        //adj list
        list<State *> succerssors = searchable->getAllPossibleStates(n);
        list<State *>::iterator it = succerssors.begin();

        for (; it != succerssors.end(); it++) {

            if (closed.find(*it) == closed.end() && !priorityQueue->contains(*it)) {
                (*it)->addSumRoute(n->getSumRoute());
                (*it)->setCameFrom(n);
                priorityQueue->push(*it);
            } else {
                sumUntil = n->getSumRoute() + (*it)->getCost();

                if (sumUntil < (*it)->getSumRoute()) {
                    (*it)->setSumRoute(sumUntil);

                    if (!priorityQueue->contains(*it)) {
                        priorityQueue->push(*it);
                    } else {
                        priorityQueue->adjust();
                    }
                }
            }
        }
    }
    return NULL;
}
