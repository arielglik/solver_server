

#ifndef SERVER_SIDE_SEARCHERINTERFACE_H
#define SERVER_SIDE_SEARCHERINTERFACE_H

#include "State.h"
#include "SearchableInterface.h"


class SearcherInterface {
public:
    virtual SearchResult *search(SearchableInterface *searchable) = 0;

    virtual int getNumberOfNodesEvaluated() = 0;

    virtual double getFinalRouteSum() const = 0;

    virtual ~SearcherInterface() {};
};

#endif //SERVER_SIDE_SEARCHERINTERFACE_H
