
#include "DepthFirstSearch.h"
/**
 * search by DepthFirstSearch
 * @param searchable
 * @return search the graph
 */
SearchResult * DepthFirstSearch::search(SearchableInterface *searchable) {
    this->searchable = searchable;
    recursiveDFS(searchable->getInitialState());
    return searchResult;
}
/**
 * recursiveDFS
 * @param s
 */
void DepthFirstSearch::recursiveDFS(State *s) {
    list<State *> adj;
    // Mark the current node as visited and
    // print it
    s->setIsVisited(true);

    // Recur for all the vertices adjacent
    // to this vertex
    list<State *>::iterator it;
    adj = searchable->getAllPossibleStates(s);

    for (it = adj.begin(); it != adj.end(); ++it) {
        if (!(*it)->getIsVisited()) {
            (*it)->setCameFrom(s);
            if (*(*it) == *searchable->getGoalState() && !isSolutionFound) {
                searchResult = searchable->backTrace(*it);
                this->finalRouteSum = searchResult->shortestPathWeight;
                this->evaluatedNodes = searchResult->developedVertices;
                isSolutionFound = true;
            }
            recursiveDFS(*it);
        }
    }
}
/**
 * constructor
 */
DepthFirstSearch::DepthFirstSearch() {
    isSolutionFound = false;
}

/**
 * destructor
 */
DepthFirstSearch::~DepthFirstSearch() {
    delete (searchResult);
}


