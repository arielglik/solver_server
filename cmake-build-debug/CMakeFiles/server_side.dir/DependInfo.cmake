# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/nir/ad2.1/AStar.cpp" "/home/nir/ad2.1/cmake-build-debug/CMakeFiles/server_side.dir/AStar.cpp.o"
  "/home/nir/ad2.1/BestFirstSearch.cpp" "/home/nir/ad2.1/cmake-build-debug/CMakeFiles/server_side.dir/BestFirstSearch.cpp.o"
  "/home/nir/ad2.1/BreadthFirstSearch.cpp" "/home/nir/ad2.1/cmake-build-debug/CMakeFiles/server_side.dir/BreadthFirstSearch.cpp.o"
  "/home/nir/ad2.1/DepthFirstSearch.cpp" "/home/nir/ad2.1/cmake-build-debug/CMakeFiles/server_side.dir/DepthFirstSearch.cpp.o"
  "/home/nir/ad2.1/FileCacheManager.cpp" "/home/nir/ad2.1/cmake-build-debug/CMakeFiles/server_side.dir/FileCacheManager.cpp.o"
  "/home/nir/ad2.1/Main.cpp" "/home/nir/ad2.1/cmake-build-debug/CMakeFiles/server_side.dir/Main.cpp.o"
  "/home/nir/ad2.1/MyClientHandler.cpp" "/home/nir/ad2.1/cmake-build-debug/CMakeFiles/server_side.dir/MyClientHandler.cpp.o"
  "/home/nir/ad2.1/MyParallelServer.cpp" "/home/nir/ad2.1/cmake-build-debug/CMakeFiles/server_side.dir/MyParallelServer.cpp.o"
  "/home/nir/ad2.1/MyPriorityQueue.cpp" "/home/nir/ad2.1/cmake-build-debug/CMakeFiles/server_side.dir/MyPriorityQueue.cpp.o"
  "/home/nir/ad2.1/SearchableStateMatrix.cpp" "/home/nir/ad2.1/cmake-build-debug/CMakeFiles/server_side.dir/SearchableStateMatrix.cpp.o"
  "/home/nir/ad2.1/Searcher.cpp" "/home/nir/ad2.1/cmake-build-debug/CMakeFiles/server_side.dir/Searcher.cpp.o"
  "/home/nir/ad2.1/StringReverser.cpp" "/home/nir/ad2.1/cmake-build-debug/CMakeFiles/server_side.dir/StringReverser.cpp.o"
  "/home/nir/ad2.1/main.cpp" "/home/nir/ad2.1/cmake-build-debug/CMakeFiles/server_side.dir/main.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
