

#include "FileCacheManager.h"

mutex mtx;

/**
 * getSolvedProblem
 * @param problem
 * @return search the problem in the map and return the solution
 */
string FileCacheManager::getSolvedProblem(string problem) {

    if (!isMapLoaded) {
        loadData();
    }
    unordered_map<string, string>::iterator it = probAndSolMap.find(problem);

    if (it == probAndSolMap.end()) {
        return "";
    } else {
        return it->second;
    }
}

/**
 * saveSolution
 * save the solution in the map
 */
void FileCacheManager::saveSolution(string problem, string solution) {
    mtx.lock();
    probAndSolMap[problem] = solution;
    mtx.unlock();
}

/**
 * constructor
 */
FileCacheManager::FileCacheManager(const string &file) : file(file) {
    isMapLoaded = false;
}

/**
 *  loadData
 * read from the file and write to the map
 */
void FileCacheManager::loadData() {
    mtx.lock();
    if (isMapLoaded) {
        return;
    }
    ifstream in(file);
    string key, value, str = "";
    string line;
    long points;

    while (getline(in, line)) {
        str += line;
        points = str.find(':');

        if (points != -1) {
            key = str.substr(0, points);
            value = str.substr(points + 1, str.length() - points);
            probAndSolMap[key] = value;
            str = "";
            continue;
        }
        str += "\n";
    }
    in.close();
    isMapLoaded = true;
    mtx.unlock();
}

/**
 * saveMap
 * write to the file
 */
void FileCacheManager::saveMap() {
    unordered_map<string, string>::iterator it = probAndSolMap.begin();
    ofstream out(file);
    string str;

    for (; it != probAndSolMap.end(); it++) {
        str = it->first + ":" + it->second;
        out << str << endl;
    }
    out.close();
}

FileCacheManager::~FileCacheManager() {
    probAndSolMap.clear();
}
