

#ifndef SERVER_SIDE_MYSPARALLELSERVER_H
#define SERVER_SIDE_MYSPARALLELSERVER_H

#include "ServerInterface.h"
#include "SearcherInterface.h"
#include "ClientHandler.h"
#include <iostream>
#include <vector>


struct ThreadParams {
    bool isFinished;
    int sockid;
    ClientHandler *clientHandler;

    ThreadParams(int sockid, ClientHandler *clientHandler);

    virtual ~ThreadParams();
};

namespace server_side {
    class MyParallelServer : public ServerInterface {

    private:
        list<ThreadParams *> threadParamsList;

    public:
        void open(int port, ClientHandler *clientHandler) override;

        void closeServer(int sockfd) override;

        MyParallelServer();
    };
}


#endif //SERVER_SIDE_MYSPARALLELSERVER_H
