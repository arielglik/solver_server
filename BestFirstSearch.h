
#ifndef SERVER_SIDE_BESTFIRSTSEARCH_H
#define SERVER_SIDE_BESTFIRSTSEARCH_H

#include "Searcher.h"
#include <set>

class BestFirstSearch : public Searcher {

public:
    SearchResult* search(SearchableInterface *searchable) override;
};


#endif //SERVER_SIDE_BESTFIRSTSEARCH_H
