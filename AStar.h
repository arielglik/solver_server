
#ifndef SERVER_SIDE_ASTAR_H
#define SERVER_SIDE_ASTAR_H


#include "Searcher.h"

using namespace std;

class AStar : public Searcher {

public:
    SearchResult *search(SearchableInterface *searchable) override;

    AStar();
};


#endif //SERVER_SIDE_ASTAR_H
