

#include <unistd.h>
#include "MyClientHandler.h"

/**
 * createSearchableMatrix
 * @param str
 * @return SearchableMatrix from string
 */
SearchableInterface *MyClientHandler::createSearchableMatrix(string str) {
    vector<string> vec;
    std::stringstream ss(str);
    std::string to;

    //split by lines
    while (std::getline(ss, to, '\n')) {
        if (to == "end") {
            break;
        }
        vec.push_back(to);
    }

    SearchableInterface *searchableInterface = new SearchableStateMatrix(vec);
    return searchableInterface;
}

/**
 * handleClient
 * this func take client problem,solve it and sent it back to the socket
 */
void MyClientHandler::handleClient(int sockfd) {
    SearchResult *searchResult;
    SearchableInterface *searchableInterface;
    string solution;
    string problem = proplemReader(sockfd);
    problem.erase(remove(problem.begin(), problem.end(), ' '), problem.end());
    solution = cm->getSolvedProblem(problem);

    if (!solution.empty()) {
        solutionWriter(sockfd, solution);
    } else {
        searchableInterface = createSearchableMatrix(problem);
        searchResult = solver->solve(searchableInterface);
        solutionWriter(sockfd, searchResult->shortestPathRoute);
        cm->saveSolution(problem, searchResult->shortestPathRoute);
        delete (searchableInterface);
        delete (searchResult);
    }
}

/**
 * proplemReader
 * reading the problem from socket
 */
string MyClientHandler::proplemReader(int sockfd) {
    string text = "";
    char buffer[256];
    int n;

    while (true) {
        std::size_t found = text.find("end");
        if (found != std::string::npos) {
            break;
        }

        bzero(buffer, 256);
        n = read(sockfd, buffer, 256);

        if (n < 0) {
            int error = errno;
            if (errno == EWOULDBLOCK) {
                continue;
            } else if (errno == 4) {
                continue;
            } else {
                perror("ERROR reading from socket");
                exit(1);
            }
        }
        string str(buffer);

        if (text.length() == (text + str).length()) {
            break;
        }
        text += str;
    }

    return text;
}

/**
 * solutionWriter
 * @param sockfd
 * @param solution
 * write the solution to the socket
 */
void MyClientHandler::solutionWriter(int sockfd, string solution) {
    char buffer[256];
    int n;
    solution += "\r\n";
    bzero(buffer, 256);

    for (int i = 0; i < solution.length(); i++) {
        buffer[i] = solution[i];
    }

    n = write(sockfd, buffer, strlen(buffer));

    if (n < 0) {
        perror("ERROR writing to socket");
        exit(1);
    }
}

MyClientHandler::MyClientHandler(Solver<SearchableInterface *, SearchResult *> *solver, CacheManager *cm) : solver(
        solver), cm(cm) {}

MyClientHandler::~MyClientHandler() {
    cm->saveMap();
    delete (solver);
    delete (cm);
}
