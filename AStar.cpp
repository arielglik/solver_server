#include "AStar.h"

/**
 * search A-star
 * @param searchable
 * @return the shortest path
 */
SearchResult *AStar::search(SearchableInterface *searchable) {
    double sumUntil;
    priorityQueue->setCompareType(CMP_BY_FCOST);
    State *node_current;
    set<State *> closed;
    State *initial = searchable->getInitialState();
    State *goal = searchable->getGoalState();
    initial->setFCost(initial->getHCost());
    priorityQueue->push(initial);


    while (!priorityQueue->isEmpty()) {
        node_current = popPriorityQueue();
        node_current->setIsVisited(true);

        // if this node is the target
        if (*node_current == *goal) {
            SearchResult *searchResult = searchable->backTrace(node_current);
            this->finalRouteSum = searchResult->shortestPathWeight;
            this->evaluatedNodes = searchResult->developedVertices;
            return searchResult;
        }

        //adj list
        list<State *> succerssors = searchable->getAllPossibleStates(node_current);
        list<State *>::iterator it = succerssors.begin();

        for (; it != succerssors.end(); it++) {
            sumUntil = node_current->getSumRoute() + (*it)->getCost();

            if (priorityQueue->contains(*it)) {
                if ((*it)->getSumRoute() <= sumUntil) {
                    continue;
                }
            } else if (closed.find(*it) != closed.end()) {
                if ((*it)->getSumRoute() <= sumUntil) {
                    continue;
                }
                closed.erase(*it);
                (*it)->setIsVisited(false);
                priorityQueue->push(*it);
            } else {
                priorityQueue->push(*it);
            }
            (*it)->setSumRoute(sumUntil);
            (*it)->setCameFrom(node_current);
        }
        closed.insert(node_current);
        priorityQueue->adjust();
    }
    if (node_current != goal) {
        throw "no route";
    }
}

AStar::AStar() {}
